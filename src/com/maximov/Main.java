package com.maximov;

import java.util.Random;

public class Main {
    private static final int NUMBER_RANGE = 100;
    private static NumbersStorage numbersStorage = new NumbersStorage(NUMBER_RANGE);
    private static Object lockObject = new Object();
    private static boolean stopFlag = false;

    public static void main(String[] args) {

        Thread numberGenerator = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Random rnd = new Random();
                    while(true) {
                        int number = rnd.nextInt(NUMBER_RANGE);
                        System.out.println(number);

                        synchronized (lockObject) {
                            boolean result = numbersStorage.tryAdd(number);
                            if(!result){
                                stopFlag = true;
                                System.out.println("Число " + number + " сгенерировано " + numbersStorage.DUBLICATE_LIMIT + " раз.");
                                return;
                            }

                        }
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread numberPrinter = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        Thread.sleep(5000);
                        synchronized (lockObject) {
                            if (stopFlag) return;

                            System.out.println("Отчёт:\n" + numbersStorage.getReport());
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        numberGenerator.start();
        numberPrinter.start();
    }
}
