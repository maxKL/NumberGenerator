package com.maximov;

import java.util.HashMap;

public class NumbersStorage {
    public final int DUBLICATE_LIMIT = 5;
    private volatile HashMap <Integer, Integer> numbersMap;
    private boolean lockStorage = false;

    public NumbersStorage(int capacity){
        numbersMap = new HashMap<>(capacity);
    }

    public boolean tryAdd(int number){
        if(lockStorage)
            return false;

        int countNumber = 0;
        if(numbersMap.containsKey(number))
            countNumber = numbersMap.get(number);

        if(++countNumber >= DUBLICATE_LIMIT) {
            lockStorage = true;
            return false;
        }
        numbersMap.put(number, countNumber);
        return true;
    }

    public String getReport(){
        String report = "";
        for(Integer key : numbersMap.keySet()){
            report += (key + ": " + numbersMap.get(key) + "\n");
        }
        return report;
    }

}
